up: docker-up
down: docker-down
restart: down up
init: clear-ready docker-down docker-pull docker-build docker-up backend-init frontend-init
frontend-init: yarn-install create-ready
backend-init: composer-install wait-db migrations fixtures
backend-test: test

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

clear-ready:
	docker run --rm -v ${PWD}/frontend:/app --workdir=/app alpine rm -f .ready

create-ready:
	docker run --rm -v ${PWD}/frontend:/app --workdir=/app alpine touch .ready

yarn-install:
	docker-compose run --rm node yarn install

cli:
	docker-compose run --rm php-cli php bin/app.php

composer-install:
	docker-compose run --rm php-cli composer install

wait-db:
	until docker-compose exec -T mysql mysqladmin ping --silent ; do sleep 1; done

migrations-diff:
	docker-compose run --rm php-cli php bin/console doctrine:migrations:diff

migrations:
	docker-compose run --rm php-cli php bin/console doctrine:migrations:migrate --no-interaction

fixtures:
	docker-compose run --rm php-cli php bin/console doctrine:fixtures:load --no-interaction

test:
	docker-compose run --rm php-cli php vendor/bin/phpunit
