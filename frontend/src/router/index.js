import { createRouter, createWebHistory } from 'vue-router'
import HomeView from "@/views/HomeView";
import CategoriesView from "@/views/CategoriesView";
import CategoryView from "@/views/CategoryView";

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/category',
    name: 'category',
    component: CategoriesView
  },
  {
    path: '/about',
    name: 'about',
    component: CategoryView
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
