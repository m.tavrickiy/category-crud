<?php

namespace App\Model\Category\Entity;

use App\Model\Common\Entity\Id;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;

class CategoryRepository
{
    private EntityManagerInterface $entityManager;
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(Category::class);
    }

    public function get(Id $id): Category
    {
        if (!$category = $this->repository->find($id->getValue())) {
            throw new EntityNotFoundException('Категория не найдена.');
        }

        return $category;
    }

    public function add(Category $category): void
    {
        $this->entityManager->persist($category);
    }

    public function remove(Category $category): void
    {
        $this->entityManager->remove($category);
    }
}
