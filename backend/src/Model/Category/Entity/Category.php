<?php

namespace App\Model\Category\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Model\Common\Entity\Id;

/**
 * @ORM\Entity
 * @ORM\Table(name="categories")
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\Column(type="id")
     */
    private Id $id;
    /**
     * @ORM\OneToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="cascade")
     */
    private ?self $parent;
    /**
     * @ORM\Column(type="string", nullable=false, name="value")
     */
    private string $value;

    public function __construct(Id $id, ?self $parent, string $value)
    {
        $this->id = $id;
        $this->parent = $parent;
        $this->value = $value;
    }

    public function edit(?self $parent, string $value): void
    {
        $this->parent = $parent;
        $this->value = $value;
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return Category|null
     */
    public function getParent(): ?Category
    {
        return $this->parent;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}
