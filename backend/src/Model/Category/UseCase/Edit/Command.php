<?php

namespace App\Model\Category\UseCase\Edit;

class Command
{
    public string $id;
    public ?string $parentId;
    public string $value;

    public function __construct(string $id, ?string $parentId, string $value)
    {
        $this->id = $id;
        $this->parentId = $parentId;
        $this->value = $value;
    }
}
