<?php

namespace App\Model\Category\UseCase\Edit;

use App\Model\Category\Entity\Category;
use App\Model\Category\Entity\CategoryRepository;
use App\Model\Common\Entity\Id;
use App\Model\Flusher;

class Handler
{
    private CategoryRepository $repository;
    private Flusher            $flusher;

    public function __construct(CategoryRepository $repository, Flusher $flusher)
    {
        $this->repository = $repository;
        $this->flusher = $flusher;
    }

    /**
     * @param Command $command
     * @return void
     */
    public function handle(Command $command): void
    {
        $parent = null;
        if (!is_null($command->parentId)) {
            $parent = $this->repository->get(new Id($command->parentId));
        }

        $category = $this->repository->get(new Id($command->id));
        $category->edit($parent, $command->value);
        $this->flusher->flush();
    }
}
