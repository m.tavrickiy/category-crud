<?php

namespace App\Model\Category\UseCase\Remove;

class Command
{
    public string $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }
}
