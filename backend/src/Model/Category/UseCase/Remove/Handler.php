<?php

namespace App\Model\Category\UseCase\Remove;

use App\Model\Category\Entity\Category;
use App\Model\Category\Entity\CategoryRepository;
use App\Model\Common\Entity\Id;
use App\Model\Flusher;

class Handler
{
    private CategoryRepository $repository;
    private Flusher            $flusher;

    public function __construct(CategoryRepository $repository, Flusher $flusher)
    {
        $this->repository = $repository;
        $this->flusher = $flusher;
    }

    /**
     * @param Command $command
     * @return void
     */
    public function handle(Command $command): void
    {
        $category = $this->repository->get(new Id($command->id));
        $this->repository->remove($category);
        $this->flusher->flush();
    }
}
