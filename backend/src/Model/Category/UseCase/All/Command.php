<?php

namespace App\Model\Category\UseCase\All;

class Command
{
    public ?string $parentId = null;
    public int     $page     = 1;
    public int     $limit    = 10;

    public function __construct(?string $parentId, int $page, int $limit)
    {
        $this->parentId = $parentId;
        $this->page = $page;
        $this->limit = $limit;
    }
}
