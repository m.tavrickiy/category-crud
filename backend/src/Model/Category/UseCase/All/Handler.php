<?php

namespace App\Model\Category\UseCase\All;

use App\Model\Common\Entity\Id;
use App\ReadModel\Category\CategoryFetcher;
use Dto\PaginationDto;

class Handler
{
    private CategoryFetcher $fetcher;

    public function __construct(CategoryFetcher $fetcher)
    {
        $this->fetcher = $fetcher;
    }

    /**
     * @param Command $command
     * @return PaginationDto
     */
    public function handle(Command $command): PaginationDto
    {
        $parentId = !is_null($command->parentId) ? new Id($command->parentId) : null;

        return new PaginationDto([
            'page'  => $command->page,
            'limit' => $command->limit,
            'total' => $this->fetcher->findAllTotalByParent($parentId),
            'data'  => $this->fetcher->findAllByParent($parentId, $command->page, $command->limit),
        ]);
    }
}
