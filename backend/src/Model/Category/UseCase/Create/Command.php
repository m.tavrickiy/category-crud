<?php

namespace App\Model\Category\UseCase\Create;

class Command
{
    public ?string $parentId;
    public string $value;

    public function __construct(?string $parentId, string $value)
    {
        $this->parentId = $parentId;
        $this->value = $value;
    }
}
