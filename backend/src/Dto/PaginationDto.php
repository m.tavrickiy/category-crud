<?php

namespace Dto;

use Spatie\DataTransferObject\DataTransferObject;

class PaginationDto extends DataTransferObject
{
    public int   $page;
    public int   $limit;
    public int   $total;
    public array $data;
}
