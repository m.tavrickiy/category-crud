<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Category\UseCase;

class CategoryController extends AbstractController
{
    #[Route('/api/category', name: 'category.all', methods: ['GET'])]
    public function all(UseCase\All\Handler $handler, Request $request)
    {
        return $this->json(
            $handler->handle(
                new UseCase\All\Command(
                    $request->query->get('parent_id'),
                    $request->query->get('page') ?? 1,
                    $request->query->get('limit') ?? 10
                )
            )
        );
    }

    #[Route('/api/category', name: 'monitoring.create', methods: ['POST'])]
    public function create(Request $request, UseCase\Create\Handler $handler)
    {
        $params = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $handler->handle(
            new UseCase\Create\Command(
                $params['parent_id'] ?? null,
                $params['value'],
            )
        );

        return $this->json([]);
    }

    #[Route('/api/category/{id}', name: 'category.edit', methods: ['PUT'])]
    public function edit(string $id, Request $request, UseCase\Edit\Handler $handler)
    {
        $params = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $handler->handle(
            new UseCase\Edit\Command(
                $id,
                $params['parent_id'] ?? null,
                $params['value'],
            )
        );

        return $this->json([]);
    }

    #[Route('/api/category/{id}', name: 'category.delete', methods: ['DELETE'])]
    public function remove(string $id, UseCase\Remove\Handler $handler)
    {
        $handler->handle(
            new UseCase\Remove\Command($id)
        );

        return $this->json([]);
    }
}