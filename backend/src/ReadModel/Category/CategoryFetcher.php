<?php

namespace App\ReadModel\Category;

use App\Model\Common\Entity\Id;
use Doctrine\DBAL\Connection;

class CategoryFetcher
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function findAllByParent(?Id $parentId, int $page = 1, int $limit = 10): array
    {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->from('categories')
            ->orderBy('value')
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit);

        if (is_null($parentId)) {
            $query->where('parent_id IS NULL');
        } else {
            $query->where('parent_id = :parentId');
            $query->setParameter('parentId', $parentId->getValue());
        }
        $rows = $query->executeQuery()->fetchAllAssociative();

        return array_map(
            static function (array $row) {
                return CategoryView::fromArray($row);
            },
            $rows
        );
    }

    public function findAllTotalByParent(?Id $parentId): int
    {
        $query = $this->connection->createQueryBuilder()
            ->select('count(*)')
            ->from('categories')
            ->orderBy('value');

        if (is_null($parentId)) {
            $query->where('parent_id IS NULL');
        } else {
            $query->where('parent_id = :parentId');
            $query->setParameter('parentId', $parentId->getValue());
        }

        return $query->executeQuery()->fetchNumeric()[0];
    }
}
