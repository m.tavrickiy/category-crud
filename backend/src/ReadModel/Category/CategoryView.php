<?php

namespace App\ReadModel\Category;

class CategoryView
{
    public string  $id;
    public ?string $parentId;
    public string  $value;

    public function __construct(string $id, ?string $parentId, string $value)
    {
        $this->id = $id;
        $this->parentId = $parentId;
        $this->value = $value;
    }

    public static function fromArray(array $category): self
    {
        return new self($category['id'], $category['parent_id'], $category['value']);
    }
}
