<?php

namespace App\DataFixtures;

use App\Model\Category\Entity\Category;
use App\Model\Common\Entity\Id;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $categories = [];

        for ($i = 0; $i < 100; $i++) {
            $parent = $this->randomParent($categories);
            $category = new Category(Id::next(), $parent, "Category $i");
            $manager->persist($category);
            $categories[] = $category;
        }

        $manager->flush();
    }

    private function randomParent(array $categories): ?Category
    {
        if (count($categories) === 0 || $this->randomIsParent()) {
            return null;
        }
        $randomParentIndex = random_int(0, count($categories) - 1);

        return $categories[$randomParentIndex];
    }

    private function randomIsParent(): bool
    {
        return random_int(0, 9) === 9;
    }
}
