<?php

namespace Unit\Category;

use App\Model\Category\Entity\Category;
use App\Model\Common\Entity\Id;
use PHPUnit\Framework\TestCase;

final class CategoryTest extends TestCase
{
    public function testCreateWithParent(): void
    {
        $parent = new Category(Id::next(), null, 'Parent');
        $category = new Category($id = Id::next(), $parent, $value = 'Child');

        $this->assertSame($id->getValue(), $category->getId()->getValue());
        $this->assertSame($parent->getId()->getValue(), $category->getParent()->getId()->getValue());
        $this->assertSame($value, $category->getValue());
    }

    public function testCreateWithoutParent(): void
    {
        $category = new Category($id = Id::next(), null, $value = 'Child');

        $this->assertSame($id->getValue(), $category->getId()->getValue());
        $this->assertNull($category->getParent());
        $this->assertSame($value, $category->getValue());
    }

    public function testEdit(): void
    {
        $parent = new Category(Id::next(), null, 'Parent');
        $category = new Category($id = Id::next(), $parent, 'Child');
        $newParent = new Category(Id::next(), null, 'New Parent');

        $category->edit($newParent, $newValue = 'New Value');

        $this->assertSame($newParent->getId()->getValue(), $category->getParent()->getId()->getValue());
        $this->assertSame($newValue, $category->getValue());
    }
}